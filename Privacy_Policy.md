# Privacy Policy

Last updated **October 04, 2022**

This privacy notice for Individual ("Company," "we," "us," or "our"), describes how and why we might collect, store, use, and/or share ("process") your information when you use our services ("Services"), such as when you:
Download and use our mobile application (Interval Timer), or any other application of ours that links to this privacy notice
Engage with us in other related ways, marketing, or events.


## Summary of Key Points

This summary provides key points from our privacy notice, but you can find out more details about any of these topics by clicking the link following each key point or by using our table of contents below to find the section you are looking for.

>**What personal information do we process?**
>
>We do not save or ask for any personal information in any shape or form.

>**Do we process any sensitive personal information?**
>
> We do not process sensitive personal information.

>**Do we receive any information from third parties?**
>
> We do not receive any information from third parties.

>**How do we process your information?**
>
> We do not request or store any personal information to process.

>**In what situations and with which parties do we share personal information?**
>
> We do not request or store any personal information to share with any outside parties.

## TABLE OF CONTENTS

1. WHAT INFORMATION DO WE COLLECT?
2. HOW DO WE PROCESS YOUR INFORMATION?
3. WHAT LEGAL BASES DO WE RELY ON TO PROCESS YOUR PERSONAL INFORMATION?
4. WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?
5. WHAT IS OUR STANCE ON THIRD-PARTY WEBSITES?
6. HOW LONG DO WE KEEP YOUR INFORMATION?
7. CONTROLS FOR DO-NOT-TRACK FEATURES
8. DO WE MAKE UPDATES TO THIS NOTICE?

## 1. WHAT INFORMATION DO WE COLLECT?

Personal information. We do not collect personal information.

Sensitive Information. We do not collect sensitive information.

Application Data. If you use our application(s), we also may collect the following information if you choose to provide us with access or permission:
Mobile Device Access. We may request access or permission to certain features from your mobile device, including your mobile device's storage, and sound. If you wish to change our access or permissions, you may do so in your device's settings.
This information is primarily needed to maintain operation of our application.

## 2. HOW DO WE PROCESS YOUR INFORMATION?

Personal information. We do not process personal information.

Sensitive Information. We do not process sensitive information.

Application Data. We only use your local mobile device's storage to keep workouts saved as inputted in the application by yourself.

## 3. WHAT LEGAL BASES DO WE RELY ON TO PROCESS YOUR INFORMATION?

We do not rely on any legal base as we do not collect or process any of your information.

## 4. WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?

As we do not collect or process your personal information, we do not share any personal information with outside parties.

## 5. WHAT IS OUR STANCE ON THIRD-PARTY WEBSITES?

In Short: We are not responsible for the safety of any information that you share with third parties that we may link to or who advertise on our Services, but are not affiliated with, our Services.

The Services may link to third-party websites, online services, or mobile applications and/or contain advertisements from third parties that are not affiliated with us and which may link to other websites, services, or applications. Accordingly, we do not make any guarantee regarding any such third parties, and we will not be liable for any loss or damage caused by the use of such third-party websites, services, or applications. The inclusion of a link towards a third-party website, service, or application does not imply an endorsement by us. We cannot guarantee the safety and privacy of data you provide to any third parties. Any data collected by third parties is not covered by this privacy notice. We are not responsible for the content or privacy and security practices and policies of any third parties, including other websites, services, or applications that may be linked to or from the Services. You should review the policies of such third parties and contact them directly to respond to your questions.

## 6. HOW LONG DO WE KEEP YOUR INFORMATION?

We do not store any personal or sensitive information as we do not request or collect any, therefore we do not store any personal information for any length of time.

## 7. CONTROLS FOR DO-NOT-TRACK FEATURES

Most web browsers and some mobile operating systems and mobile applications include a Do-Not-Track ("DNT") feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. At this stage no uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this privacy notice.

## 8. DO WE MAKE UPDATES TO THIS NOTICE?

In Short: Yes, we will update this notice as necessary to stay compliant with relevant laws.

We may update this privacy notice from time to time. The updated version will be indicated by an updated "Revised" date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy notice, we may notify you either by prominently posting a notice of such changes as an application update release note. We encourage you to review this privacy notice frequently to be informed of how we are protecting your information.
